#! /bin/bash
set -e
subscription_id="subscription_id"
VM_NAME="VM_NAME"
echo "Since we have already created a virtual machine in previous project, we are now querying metadata for the same in json format"

az_login()
{
echo "executing rest api to create az login token"
export ACCESS_TOKEN=$(curl -X --location "https://login.microsoftonline.com/common/oauth2/token")
}

query_data()
{
curl -X  https://management.azure.com/subscriptions/$subscription_id/resourceGroups/myResourceGroup/providers/Microsoft.Compute/virtualMachines/$VM_NAME?api-version=2022-03-01 -H "Authorization:Bearer $ACCESS_TOKEN"| tee -a output.log
echo $output.log
}

az_login
query_data
